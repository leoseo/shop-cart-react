import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import AuthService from "./services/auth.service";
import Login from "./components/login.component";
import Profile from "./components/profile.component";
import Register from "./components/register.component";
import Catalog from './components/catalog.component';
import Cart from './components/cart.component';
import Home from './components/home.component';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showAdminBoard: false,
      showStaffBoard: false,
      currentUser: undefined
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: AuthService.getCurrentUser(),
        showAdminBoard: user.roles.includes("ROLE_ADMINISTRATOR"),
        showStaffBoard: user.roles.includes("ROLE_STAFF")
      });
    }
  }

  logOut = (e) => {
    AuthService.logout();
    alert("Logged out!");
    this.setState({
      currentUser: undefined
    });
  }

  render() {

    return (
      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/home"} className="nav-link">Home</Link>
              </li>

              {this.state.showAdminBoard &&
                <li className="nav-item">
                  <Link to={"/admin"} className="nav-link">Admin Board</Link>
                </li>
              }

              {(this.state.showAdminBoard || this.state.showStaffBoard) &&
                <li className="nav-item">
                  <Link to={"/staff"} className="nav-link">Staff Board</Link>
                </li>
              }
              {!this.state.currentUser
                &&
                <li className="nav-item">
                  <Link to={"/catalog"} className="nav-link">Catalog</Link>
                </li>}
              {!this.state.currentUser &&
                <li className="nav-item">
                  <Link to={"/cart"} className="nav-link">My Cart</Link>
                </li>}

            </div>

            {this.state.currentUser
              ? (
                <div className="navbar-nav pull-right">
                  <li className="nav-item">
                    <Link to={"/profile"} className="nav-link">Profile</Link>
                  </li>
                  <li className="nav-item">
                    <Link to={"/logout"} className="nav-link" onClick={this.logOut}>Logout</Link>
                  </li>
                </div>
              )
              : (
                <div className="navbar-nav pull-right">

                  <li className="nav-item">
                    <Link to={"/login"} className="nav-link">Login</Link>
                  </li>
                  <li className="nav-item">
                    <Link to={"/register"} className="nav-link">Register</Link>
                  </li>
                </div>)
            }
          </nav>


          <div className="container mt-3">
            <Switch>
              {/* <Route exact path={["/", "/home"]}">
                {/* {this.state.currentUser ? <Redirect to="/profile" /> : <Redirect to="/login" />} */}
              {/*</Switch></Route> */}
              <Route exact path="/" component={Home} />
              <Route exact path="/home" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/catalog" component={Catalog} />
              <Route exact path="/cart" component={Cart} />
              {/*<Route path="/user" component={BoardUser} />
              <Route path="/mod" component={BoardModerator} />
              <Route path="/admin" component={BoardAdmin} /> */}
            </Switch>
          </div>

        </div>
      </Router >
    )
  }
}

export default App;
