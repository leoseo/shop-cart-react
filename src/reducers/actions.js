export const FETCH_PRODUCTS_PENDING = 'FETCH_PRODUCTS_PENDING';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const SUB_QUANTITY = 'SUB_QUANTITY';
export const ADD_QUANTITY = 'ADD_QUANTITY';

export function fetchProductsPending() {
    return {
        type: FETCH_PRODUCTS_PENDING
    }
}

export function fetchProductsSuccess(products) {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        products: products
    }
}

export function fetchProductsError() {
    return {
        type: FETCH_PRODUCTS_ERROR
    }
}

export function addToCart(id){
    return {
        type: ADD_TO_CART,
        id: id
    }
}

export function removeItem(id){
    return {
        type: REMOVE_ITEM,
        id: id
    }
}

export function addQuantity(id){
    return {
        type: ADD_QUANTITY,
        id: id
    }
}

export function subtractQuantity(id){
    return {
        type: SUB_QUANTITY,
        id: id
    }
}