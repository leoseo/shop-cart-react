import {
    ADD_TO_CART, REMOVE_ITEM, SUB_QUANTITY,
    ADD_QUANTITY, FETCH_PRODUCTS_PENDING,
    FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR
} from './actions'

export const initState = {
    products: [],
    addedItems: [],
    total: 0,
    pending: false,
    error: null
}

const cartReducer = (state = initState, action) => {

    console.log('current: ' + JSON.stringify(state.addedItems));
    switch (action.type) {

        // these actions should be separated to another reducer
        case FETCH_PRODUCTS_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                pending: false,
                products: action.products
            }
        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                pending: false,
                error: "Failed to load catalog!"
            }

        // these actions should be separated to another reducer
        case ADD_TO_CART:
            let addedItem = state.products.find(item => item.id === action.id)
            let existed_item = state.addedItems.find(item => item.id === action.id)
            if (existed_item) {
                addedItem.quantity += 1
                return {
                    ...state,
                    total: state.total + addedItem.price
                }
            } else {
                addedItem.quantity = 1;
                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: state.total + addedItem.price
                }

            }
        case REMOVE_ITEM:
            let itemToRemove = state.addedItems.find(item => action.id === item.id)
            let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity);
            return {
                ...state,
                addedItems: state.addedItems.filter(item => action.id !== item.id),
                total: newTotal < 0 ? 0 : newTotal
            }
        case ADD_QUANTITY:
            let addedQtyItem = state.addedItems.find(item => item.id === action.id)
            addedQtyItem.quantity += 1
            return {
                ...state,
                total: state.total + addedQtyItem.price
            }
        case SUB_QUANTITY:
            let subQtyItem = state.addedItems.find(item => item.id === action.id)
            if (subQtyItem.quantity === 1) { // when quantity deducted
                let new_items = state.addedItems.filter(item => item.id !== action.id)
                let newTotal2 = state.total - subQtyItem.price;
                return {
                    ...state,
                    addedItems: new_items,
                    total: newTotal2 < 0 ? 0 : newTotal2
                }
            } else {
                subQtyItem.quantity -= 1
                return {
                    ...state,
                    total: state.total - subQtyItem.price
                }
            }
        default:
            return state;
    }

}
export default cartReducer;