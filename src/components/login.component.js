import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from '@material-ui/core/Button';
import AuthService from "../services/auth.service";
import { required } from "../services/constraint/form.constraint";

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            loading: false,
            message: ""
        };
    }

    onChangeUsername = (e) => {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword = (e) => {
        this.setState({
            password: e.target.value
        });
    }

    clear = (e) => {
        this.setState({
            username: "",
            password: "",
            loading: false,
            message: ""
        });
    }

    handleLogin = (e) => {

        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {

            AuthService.login(this.state.username, this.state.password).then(
                (res) => {
                    if (res) {
                        alert(res);
                        this.props.history.push("/profile");
                        window.location.reload();
                    }
                },
                (error) => {
                    const restMessage = (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        loading: false,
                        message: restMessage
                    });
                }
            );

        } else {
            this.setState({
                loading: false
            });
        }

    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-4">

                        <Form
                            onSubmit={this.handleLogin}
                            ref={c => {
                                this.form = c;
                            }}
                        >

                            <div className="form-group">
                                <label htmlFor="username">Username</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.onChangeUsername}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <Input
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.onChangePassword}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <Button style={{ marginLeft: 60 }} type="submit"
                                    variant="contained" color="primary" disabled={this.state.loading}>
                                    {this.state.loading &&
                                        (<span className="spinner-border spinner-border-sm"></span>)
                                    }
                                    <span>Login</span>
                                </Button>

                                <Button style={{ marginLeft: 30 }} variant="contained" onClick={this.clear}>
                                    <span>Clear</span>
                                </Button>
                            </div>

                            {this.state.message &&
                                (<div className="form-group">
                                    <div className="alert alert-danger" role="alert">
                                        {Array.isArray(this.state.message)
                                            ? this.state.message.map((msg, index) =>
                                                <span key={index}>{msg}<br /></span>
                                            )
                                            : <span>{this.state.message}<br /></span>
                                        }
                                    </div>
                                </div>)
                            }

                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />

                        </Form>

                    </div>
                </div>
            </div>
        );
    }
}