import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { removeItem, addQuantity, subtractQuantity } from '../reducers/actions';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

class Cart extends Component {

    handleRemove = (id) => {
        this.props.removeItem(id);
    }
    handleAddQuantity = (id) => {
        this.props.addQuantity(id);
    }
    handleSubtractQuantity = (id) => {
        this.props.subtractQuantity(id);
    }

    render() {

        const { addedItems, total } = this.props;

        let content = addedItems && addedItems.length
            ? (
                addedItems.map(item => {
                    return (
                        <div class="item-desc cart-wrapper" key={item.id}>
                            <div width="180px" height="180px">
                                <img class="cart-small-image card-image item-desc gray float-left"
                                    src={process.env.PUBLIC_URL + '/images/' + item.imagePath}
                                    alt={item.img} width="150px" height="150px" />
                            </div>

                            <div class="cart-text">
                                <label><b>{item.productName}</b></label><br />
                                <label>Price: ${item.price}</label><br />
                                <label>Quantity: {item.quantity}</label><br />
                                <div style={{ textAlign: "center", fontSize: "1.5em" }}>
                                    <Link to="/cart">
                                        <b><i onClick={() => { this.handleAddQuantity(item.id) }} >+</i></b>
                                    </Link>
                                    <Link style={{ paddingLeft: "40px" }} to="/cart">
                                        <b><i onClick={() => { this.handleSubtractQuantity(item.id) }}>-</i></b>
                                    </Link><br />

                                    <Button variant="contained" color="primary"
                                        onClick={() => { this.handleRemove(item.id) }}>
                                        Remove
                                    </Button>
                                </div>

                            </div><hr />
                        </div>
                    );


                })
            )
            : (<p>Nothing</p>);

        return (
            <div className="container">
                <div className="cart">
                    <h5>You have ordered:</h5>
                    <ul className="collection">
                        {content}
                    </ul>
                    <h5>The total price is:
                        ${total === 0 ? 0 : Math.floor(total * 100) / 100}
                    </h5>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    addedItems: state.addedItems,
    total: state.total
})
const mapDispatchToProps = (dispatch) => bindActionCreators({
    removeItem: (id) => removeItem(id),
    addQuantity: (id) => addQuantity(id),
    subtractQuantity: (id) => subtractQuantity(id)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
