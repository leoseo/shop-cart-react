import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import AuthService from "../services/auth.service";
import { required, email, password, cpassword, permissions } from "../services/constraint/form.constraint";

export default class Register extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            permissions: [],
            fullName: "",
            email: "",
            phone: "",
            loading: false,
            message: ""
        }
    }

    onChangeUsername = (e) => {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword = (e) => {
        this.setState({
            password: e.target.value
        });
    }

    onChangeCPassword = (e) => {
        this.setState({
            cpassword: e.target.value
        });
    }

    onChangePermissions = (e) => {

        const tmp = this.state.permissions;
        if (e.target.checked === true) {
            tmp.push(e.target.name);
        } else {
            const index = tmp.indexOf(e.target.name);
            tmp.splice(index, 1);
        }
        console.log("after: " + this.state.permissions)
    };

    onChangeFullName = (e) => {
        this.setState({
            fullName: e.target.value
        });
    }

    onChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    onChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        });
    }

    clear = (e) => {
        this.setState({
            username: "",
            password: "",
            cpassword: "",
            permissions: [],
            fullName: "",
            email: "",
            phone: "",
            loading: false,
            message: ""
        });
    }

    handleRegister = (e) => {

        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.registerUser(this.state.username, this.state.password, this.state.permissions,
                this.state.fullName, this.state.email, this.state.phone).then(
                    (res) => {
                        if (res && res.includes("Success")) {
                            alert(res);
                            AuthService.login(this.state.username, this.state.password).then(
                                (res) => {
                                    this.props.history.push("/profile");
                                    window.location.reload();
                                }, (error) => {
                                    this.props.history.push("/login");
                                    window.location.reload();
                                }
                            );
                        }
                    }, (error) => {
                        const restMessage = (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                            error.message ||
                            error.toString();
                
                        this.setState({
                            loading: false,
                            message: restMessage
                        });
                    }
                );
        } else {
            this.setState({
                loading: false
            });
        }
    }

    render() {

        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">

                        <Form
                            onSubmit={this.handleRegister}
                            ref={c => {
                                this.form = c;
                            }}
                        >
                            <div className="form-group">
                                <label htmlFor="username">User Name</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.onChangeUsername}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <Input
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.onChangePassword}
                                    validations={[required, password]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="cpassword">Confirm Password</label>
                                <Input
                                    type="password"
                                    className="form-control"
                                    name="cpassword"
                                    value={this.state.cpassword}
                                    onChange={this.onChangeCPassword}
                                    validations={[required]}
                                />
                                <Input
                                    type="hidden"
                                    value={[this.state.password, this.state.cpassword]}
                                    validations={[cpassword]} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="permissions">Roles</label>
                                <FormGroup>
                                    <FormControlLabel
                                        control={<Checkbox onChange={this.onChangePermissions} name="ROLE_STAFF" />}
                                        label="Sale Staff"
                                    />
                                    <FormControlLabel
                                        control={<Checkbox onChange={this.onChangePermissions} name="ROLE_ADMINISTRATOR" />}
                                        label="Administrator"
                                    />
                                </FormGroup>
                                <Input
                                    name="permissions"
                                    type="hidden"
                                    value={this.state.permissions}
                                    validations={[permissions]} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="fullName">Full Name</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="fullName"
                                    value={this.state.fullName}
                                    onChange={this.onChangeFullName}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    value={this.state.email}
                                    onChange={this.onChangeEmail}
                                    validations={[required, email]}
                                />
                            </div>

                            <div className="form-group">
                                <Button
                                    style={{ marginLeft: 60 }}
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={this.state.loading}>
                                    {this.state.loading &&
                                        (<span className="spinner-border spinner-border-sm"></span>)
                                    }
                                    <span>Register</span>
                                </Button>

                                <Button
                                    style={{ marginLeft: 30 }}
                                    variant="contained"
                                    onClick={this.clear}>
                                    <span>Clear</span>
                                </Button>
                            </div>

                            {this.state.message &&
                                (<div className="form-group">
                                    <div className="alert alert-danger" role="alert">
                                        {Array.isArray(this.state.message)
                                            ? this.state.message.map((msg, index) =>
                                                <span key={index}>{msg}<br /></span>
                                            )
                                            : <span>{this.state.message}<br /></span>
                                        }
                                    </div>
                                </div>)
                            }

                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />
                        </Form>

                    </div>
                </div>
            </div>
        );
    }

}