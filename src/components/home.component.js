import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Home extends Component {

    render() {
        return (
            <div>
                <h1>Welcome to Yum Yum grocery shop!</h1>
                <br/>
                <h3 style={{marginLeft: "5px"}}>Customer please click <Link to="/catalog">here</Link> for shopping.</h3>
                <h3 style={{marginLeft: "5px"}}>Employee please click <Link to="/login">here</Link> for login.</h3>
            </div>
        )
    }
}
