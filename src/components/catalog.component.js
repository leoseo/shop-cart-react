import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CatalogService from "../services/catalog.service";
import { addToCart } from '../reducers/actions';
import { green } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import AddCircle from '@material-ui/icons/AddCircle';

require('dotenv').config();

class Catalog extends Component {

    componentWillMount() {
        const { fetchProducts } = this.props;
        fetchProducts();
    }

    shouldComponentRender = (e) => {
        return !this.props.pending;
    }

    handleClick = (id) => {
        console.log('clicked item id: ' + id);
        this.props.addToCart(id);
    }

    render() {

        const { products, error } = this.props;

        // abort loading
        if (!this.shouldComponentRender()) {
            return (<span>Loading ...</span>);
        }

        // got either errors or real products
        let content = error || !products
            ? <span className='product-list-error'>{error}</span>
            : products.map(item =>

                <div className="card" key={item.id}>
                    <div className="card-image item-desc gray">
                        <img src={process.env.PUBLIC_URL + '/images/' + item.imagePath}
                            alt={item.productName} width="200px" height="200px" />
                    </div>

                    <div className="card-content">
                        <label className="card-label bold">
                            {item.productName}</label><br />

                        <div style={{ float: "right top" }} className="detail-box">

                            <label className="padding-left">InStock: {item.availableQuantity}</label><br />
                            <label className="padding-left">Price: ${item.price}</label>
                            <IconButton className="material-icons" >
                                <AddCircle style={{
                                    color: green[500],
                                    width: "50px", height: "50px"
                                }} onClick={() => { this.handleClick(item.id) }} />
                            </IconButton>
                            <br />

                        </div>
                    </div>
                </div>


            );

        return (
            <div className="container">
                <h3 className="center">Our items</h3>
                <div className="box">
                    {content}
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    error: state.error,
    products: state.products,
    pending: state.pending
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    fetchProducts: CatalogService.fetchProducts,
    addToCart: (id) => addToCart(id)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);