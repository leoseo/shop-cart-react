import React from "react";
import validator from "validator";

export const required = val => {
    if (!val) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }

};

export const email = val => {
    if (!validator.isEmail(val)) {
        return (
            <div className="alert alert-danger" role="alert">
                Invalid email format!
            </div>
        );
    }
}

export const permissions = vals => {
    if (!Array.isArray(vals) || vals.length <= 0) {
        return (
            <div className="alert alert-danger" role="alert">
                Choose at least one role!
            </div>
        );
    }
}

export const password = val => {
    // simple password for demonstration
    const regex = "[0-9A-Za-z]{8,}";
    if (!val.match(regex)) {
        return (
            <div className="alert alert-danger" role="alert">
                Password can only contain numbers and letters.<br/>
                Password must have at least 8 characters.<br/>
            </div>
        );
    }
}

export const cpassword = vals => {
    if (vals[0] !== vals[1]) {
        return (
            <div className="alert alert-danger" role="alert">
                Password must be equal to Confirm Password.<br/>
            </div>
        );
    }
}