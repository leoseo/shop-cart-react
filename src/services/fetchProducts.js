import API from './API';
import {fetchProductsPending, fetchProductsSuccess, fetchProductsError} from '../reducers/actions';

function fetchProducts(){
    return dispatch => {
        dispatch(fetchProductsPending);
        API.get("/product/list")
        // .then(res => res.json())
        .then( res => {
            if (res.error) {
                throw (res.error);
            }

            dispatch(fetchProductsSuccess(res.data.message));
            return res.data.message;
        }
        ).catch(error => {
            console.log('testerror# ' + error);
            dispatch(fetchProductsError(error))
        })

    };
}

export default fetchProducts;

