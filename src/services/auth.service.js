import API from "./API";

class AuthService {

    login(username, password) {

        return API.post('auth/signin', {
            "username": username,
            "password": password
        }).then((response) => {
            if (response.data.message.token) {
                localStorage.setItem("user", JSON.stringify(response.data.message));
            }
            return "Successfully signed in for user " + username + "!";
        });
    }

    logout() {
        localStorage.removeItem("user");
    }

    registerUser(username, password, permissions, fullName, email, phone) {

        return API.post('auth/signup', {
            "username": username,
            "password": password,
            "permissions": permissions,
            "fullName": fullName,
            "email": email,
            "phone": phone
        }).then((res) => {
            return res.data.message;
        }
        );
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem("user"));
    }
}

export default new AuthService();