import API from "./API";
import { fetchProductsPending, fetchProductsSuccess, fetchProductsError } from '../reducers/actions';

class CatalogService {

    fetchProducts() {
        return dispatch => {
            dispatch(fetchProductsPending);
            API.get("/product/list")
                .then(res => {
                    console.log('asd' + res);
                    if (res.error) {
                        throw (res.error);
                    }
                    dispatch(fetchProductsSuccess(res.data.message));
                    return res.data.message;
                }
                ).catch(error => {
                    dispatch(fetchProductsError())
                    console.log('Encountered error: ' + error);
                })

        };
    }

}

export default new CatalogService();